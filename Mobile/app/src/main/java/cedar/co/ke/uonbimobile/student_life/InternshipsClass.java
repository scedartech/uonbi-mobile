package cedar.co.ke.uonbimobile.student_life;

/**
 * Created by kmulw on 25-Sep-17.
 */

public class InternshipsClass {
    public InternshipsClass(){}

    public String company, time, title, description;

    public InternshipsClass(String company, String time, String title, String description){
        this.company = company;
        this.time = time;
        this.title = title;
        this.description = description;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
