package cedar.co.ke.uonbimobile.login_register;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.main_activity.MainActivity;


public class LoginActivity extends AppCompatActivity {
    int LOGIN_TIME_OUT = 3000;
    EditText txPINNumber;
    EditText txPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        Button btnCreateAccount = (Button) findViewById(R.id.btnCreateAccount);
        txPINNumber = (EditText) findViewById(R.id.txRegistrationNumber);
        txPassword = (EditText) findViewById(R.id.txPassword);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final MaterialDialog progressDialog = new MaterialDialog.Builder(LoginActivity.this)
                        .title("Signing in")
                        .content("Please wait")
                        .progress(true, 100)
                        .cancelable(false)
                        .progressIndeterminateStyle(true)
                        .show();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        progressDialog.hide();
                        ActivityCompat.finishAffinity(LoginActivity.this);
                    }
                }, LOGIN_TIME_OUT);
            }
        });

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, Register.class));
                ActivityCompat.finishAffinity(LoginActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                exit();
                break;
            case R.id.action_help:
                help();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        exit();
    }

    public void exit(){
        MaterialDialog dialogExit = new MaterialDialog.Builder(LoginActivity.this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Exit")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        ActivityCompat.finishAffinity(LoginActivity.this);
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                    }
                })
                .show();
    }

    public void help(){
        MaterialDialog dialogHelp = new MaterialDialog.Builder(LoginActivity.this)
                .title("Help")
                .content("University of Nairobi Login page help content.")
                .negativeText("Close")
                .canceledOnTouchOutside(false)
                .show();
    }
}

