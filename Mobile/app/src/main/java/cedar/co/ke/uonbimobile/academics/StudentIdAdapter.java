package cedar.co.ke.uonbimobile.academics;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class StudentIdAdapter extends RecyclerView.Adapter<StudentIdAdapter.MyViewHolder> {

    private List<StudentIdClass> studentIdList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout llStatus;
        TextView validity, status, date;

        public MyViewHolder(View itemView) {
            super(itemView);
            validity = (TextView) itemView.findViewById(R.id.validity);
            status = (TextView) itemView.findViewById(R.id.status);
            date = (TextView) itemView.findViewById(R.id.date);
            llStatus = (LinearLayout) itemView.findViewById(R.id.llStatus);
        }
    }

    StudentIdAdapter(List<StudentIdClass> studentIdList){ this.studentIdList = studentIdList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewGrade) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_studentid_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        StudentIdClass studentIdClass = studentIdList.get(position);
        holder.validity.setText(studentIdClass.getValidity());
        holder.status.setText(studentIdClass.getStatus());
        holder.date.setText(studentIdClass.getDate());

        if(studentIdClass.getRemarks().equals("1")){
            holder.llStatus.setBackgroundResource(R.color.colorAttencance90);
        } else {
            holder.llStatus.setBackgroundResource(R.color.colorAttencance40);
        }
    }

    @Override
    public int getItemCount() { return studentIdList.size();}
}
