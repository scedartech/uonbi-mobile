package cedar.co.ke.uonbimobile.accomodation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import cedar.co.ke.uonbimobile.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccomodationMainNavigatorTab3Fragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    MapView mMapView;
    private GoogleMap googleMap;
    private GoogleMap mMap;
    LatLng location;
    String breedHeader;
    int iterator;

    public AccomodationMainNavigatorTab3Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_accomodation_main_navigator_tab3, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(AccomodationMainNavigatorTab3Fragment.this);

        return rootView;
    }

    /** Called when the map is ready. */
    @Override
    public void onMapReady(GoogleMap map) {
        mMap = map;

        int height = 100;
        int width = 100;

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        LatLngBounds bounds = builder.build();

        int mapWidth = getResources().getDisplayMetrics().widthPixels;
        int mapHeight = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (mapWidth * 0.30); // offset from edges of the map 30% of screen
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, mapWidth, mapHeight, padding);
        mMap.animateCamera(cu);

        // Set a listener for marker click.
        mMap.setOnMarkerClickListener(this);
    }

    /**Called when the user clicks a marker. */
    @Override
    public boolean onMarkerClick(final Marker marker) {
        return false;
    }

}
