package cedar.co.ke.uonbimobile.main_activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;


public class TimetableAdapter extends RecyclerView.Adapter<TimetableAdapter.MyViewHolder> {

    private List<TimetableClass> timetableList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView course, time;

        public MyViewHolder(View itemView) {
            super(itemView);
            course = (TextView) itemView.findViewById(R.id.course);
            time = (TextView) itemView.findViewById(R.id.time);
        }
    }

    public TimetableAdapter(List<TimetableClass> timetableList){ this.timetableList = timetableList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_timetable_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TimetableClass timetableClass = timetableList.get(position);
        holder.course.setText(timetableClass.getCourse());
        holder.time.setText(timetableClass.getTime());
    }

    @Override
    public int getItemCount() { return timetableList.size();}
}
