package cedar.co.ke.uonbimobile.student_life;


public class RemindersClass {
    public RemindersClass(){}
    public String title, content, time;
    public RemindersClass(String title, String content, String time){
        this.title = title;
        this.content = content;
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
