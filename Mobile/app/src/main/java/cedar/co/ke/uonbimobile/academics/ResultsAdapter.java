package cedar.co.ke.uonbimobile.academics;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class ResultsAdapter extends RecyclerView.Adapter<ResultsAdapter.MyViewHolder> {

    private List<ResultsClass> resultsList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout llGrade;
        TextView title, code, grade;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            code = (TextView) itemView.findViewById(R.id.code);
            grade = (TextView) itemView.findViewById(R.id.grade);
        }
    }

    ResultsAdapter(List<ResultsClass> resultsList){ this.resultsList = resultsList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewGrade) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_results_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ResultsClass resultsClass = resultsList.get(position);
        holder.title.setText(resultsClass.getTitle());
        holder.code.setText(resultsClass.getCode());
        holder.grade.setText(resultsClass.getGrade());
    }

    @Override
    public int getItemCount() { return resultsList.size();}
}
