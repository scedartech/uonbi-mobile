package cedar.co.ke.uonbimobile.academics;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.login_register.LoginActivity;

public class AcademicsCourseRegistrationMain extends AppCompatActivity {
    private List<RegisteredCoursesClass> courseList = new ArrayList<>();
    private RegisteredCoursesAdapter courseAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academics_course_registration_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Registered Courses");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        courseAdapter = new RegisteredCoursesAdapter(courseList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(courseAdapter);
        prepareRecyclerViewData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AcademicsCourseRegistrationMain.this, AcademicsRegisterCourse.class));
            }
        });
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>"+
                "<COURSES>"+
                "<COURSE TITLE='Analysis and design of algorithms' CODE='CSC311'/>"+
                "<COURSE TITLE='Artificial Intelligence Programming' CODE='CSC312'/>"+
                "<COURSE TITLE='Foundations of HCI' CODE='CSC313'/>"+
                "<COURSE TITLE='Computer Graphics' CODE='CSC314'/>"+
                "<COURSE TITLE='Distributed Systems' CODE='CSC315'/>"+
                "<COURSE TITLE='Introductions to Orgs and Mgmt.' CODE='CSC316'/>"+
                "</COURSES>";
        RegisteredCoursesClass courseClass;
        DocumentBuilder builder = null;
        System.out.println(xml);
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("COURSE").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            courseClass = new RegisteredCoursesClass(
                    (doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim(),
                    (doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("CODE").getTextContent()).trim());
            System.out.println("title is "+doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("TITLE").getTextContent());
            courseList.add(courseClass);
        }
        courseAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.subpage, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_help) {
            new MaterialDialog.Builder(this)
                    .title("Help")
                    .content("This page allows you to create an account that you can login with every time you want to use this mobile applicaton. Please note your username and password.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        }  else if(id == R.id.action_log_out){
            startActivity(new Intent(this, LoginActivity.class));
            ActivityCompat.finishAffinity(this);
        }

        return super.onOptionsItemSelected(item);
    }

    void exitQuestion(){
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Yes")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        ActivityCompat.finishAffinity(AcademicsCourseRegistrationMain.this);
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, AcademicsMainNavigator.class));
        ActivityCompat.finishAffinity(this);
    }
}
