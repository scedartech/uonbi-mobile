package cedar.co.ke.uonbimobile.messaging;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;

public class MessagingTab1Fragment extends Fragment {
    private List<MessagingClassMessagesClass> messagesList = new ArrayList<>();
    private MessagingClassMessagesAdapter messagesAdapter;

    public MessagingTab1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_messaging_tab1, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        messagesAdapter = new MessagingClassMessagesAdapter(messagesList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(messagesAdapter);
        prepareRecyclerViewData();

        return rootView;
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>" +
                "<RESPONSE>"+
                "<MESSAGES>"+
                "<MESSAGE SENDER='Kiptoo Mulwa' TITLE='In the data files' TIME='12.43 PM' CONTENT='In the element, the format file represents the data values in all three fields as character data. For each field, the TERMINATOR attribute indicates the terminator that follows the data value.'/>" +
                "<MESSAGE SENDER='Kiptoo Mulwa' TITLE='The following example' TIME='9.15 AM' CONTENT='It will also take text and attribute arguments (after the tag name) like normal markup methods. (But see the next bullet item for a better way to handle XML namespaces). Direct support for XML namespaces is now available. If the first argument to a tag call is a symbol, it will be joined to the tag to produce a namespace combination. It is easier to show this than describe it.'/>" +
                "<MESSAGE SENDER='Kiptoo Mulwa' TITLE='Table (row)' TIME='12.56 PM' CONTENT='The instance_eval implementation which forces self to refer to the message receiver as self is now obsolete. We now use normal block calls to execute the markup block. This means that all markup methods must now be explicitly send to the xml builder. For instance, instead of '/>" +
                "<MESSAGE SENDER='Kiptoo Mulwa' TITLE='The data fields' TIME='12.43 PM' CONTENT='Although more verbose, the subtle change in semantics within the block was found to be prone to error. To make this change a little less cumbersome, the markup block now gets the markup object sent as an argument, allowing you to use a shorter alias within the block. '/>" +
                "<MESSAGE SENDER='Kiptoo Mulwa' TITLE='Ordering data fields' TIME='5.43 PM' CONTENT='The special XML characters automatically. Use the operation to insert text without modification.'/>" +
                "<MESSAGE SENDER='Kiptoo Mulwa' TITLE='The following example' TIME='6.43 AM' CONTENT='Create XML markup easily. All (well, almost all) methods sent to an XmlMarkup object will be translated to the equivalent XML markup. Any method with a block will be treated as an XML markup tag with nested markup in the block. '/>" +
                "</MESSAGES>" +
                "</RESPONSE>";
        MessagingClassMessagesClass messagesClass;
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("MESSAGE").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            messagesClass = new MessagingClassMessagesClass(
                    (doc.getElementsByTagName("MESSAGE").item(i).getAttributes().getNamedItem("SENDER").getTextContent()).trim(),
                    (doc.getElementsByTagName("MESSAGE").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim(),
                    (doc.getElementsByTagName("MESSAGE").item(i).getAttributes().getNamedItem("TIME").getTextContent()).trim(),
                    (doc.getElementsByTagName("MESSAGE").item(i).getAttributes().getNamedItem("CONTENT").getTextContent()).trim()
            );
            messagesList.add(messagesClass);
        }
        messagesAdapter.notifyDataSetChanged();
    }
}
