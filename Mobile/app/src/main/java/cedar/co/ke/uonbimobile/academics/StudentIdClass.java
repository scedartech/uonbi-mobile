package cedar.co.ke.uonbimobile.academics;

public class StudentIdClass {
    private String date, status, validity, remarks;
    public StudentIdClass(){};

    public StudentIdClass(String date, String status, String validity, String remarks){
        this.date = date;
        this.status = status;
        this.validity = validity;
        this.remarks = remarks;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }
}
