package cedar.co.ke.uonbimobile.messaging;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.login_register.LoginActivity;

public class MessagingComposeMessage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messaging_compose_message);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Compose Message");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Button btnRegister = (Button) findViewById(R.id.btnRegister);
        Button btnBack = (Button) findViewById(R.id.btnBack);

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressBar pbLoadSpinner = (ProgressBar) findViewById(R.id.pbLoadSpinner);
                pbLoadSpinner.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        pbLoadSpinner.setVisibility(View.GONE);
                        MaterialDialog msg = new MaterialDialog.Builder(MessagingComposeMessage.this)
                                .title("Success")
                                .content("Message sent successfully")
                                .positiveText("Close")
                                .canceledOnTouchOutside(false)
                                .onPositive(new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                        ActivityCompat.finishAffinity(MessagingComposeMessage.this);
                                        startActivity(new Intent(MessagingComposeMessage.this, MessagingMain.class));
                                        finish();
                                    }
                                })
                                .show();
                    }
                }, 2400);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MessagingComposeMessage.this, MessagingMain.class));
                finish();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.subpage, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_help) {
            new MaterialDialog.Builder(this)
                    .title("Help")
                    .content("This page allows you to create an account that you can login with every time you want to use this mobile applicaton. Please note your username and password.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        }  else if(id == R.id.action_log_out){
            startActivity(new Intent(this, LoginActivity.class));
            ActivityCompat.finishAffinity(this);
        }

        return super.onOptionsItemSelected(item);
    }

    void exitQuestion(){
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Yes")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        ActivityCompat.finishAffinity(MessagingComposeMessage.this);
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, MessagingMain.class));
        finish();
    }
}
