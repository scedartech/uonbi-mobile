package cedar.co.ke.uonbimobile.messaging;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class MessagingSchoolMailAdapter extends RecyclerView.Adapter<MessagingSchoolMailAdapter.MyViewHolder> {

    private List<MessagingSchoolMailClass> messagesList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView type;
        TextView sender, title, time, content;

        public MyViewHolder(View itemView) {
            super(itemView);
            type = (ImageView) itemView.findViewById(R.id.type);
            sender = (TextView) itemView.findViewById(R.id.sender);
            title = (TextView) itemView.findViewById(R.id.title);
            time = (TextView) itemView.findViewById(R.id.time);
            content = (TextView) itemView.findViewById(R.id.content);
        }
    }

    MessagingSchoolMailAdapter(List<MessagingSchoolMailClass> messagesList){ this.messagesList = messagesList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_messaging_class_message, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        MessagingSchoolMailClass messagesClass = messagesList.get(position);
        holder.sender.setText(messagesClass.getSender());
        holder.title.setText(messagesClass.getTitle());
        holder.time.setText(messagesClass.getTime());
        holder.content.setText(messagesClass.getContent());
        holder.type.setImageResource(R.drawable.ic_messages_school_mail);
    }

    @Override
    public int getItemCount() { return messagesList.size();}
}
