package cedar.co.ke.uonbimobile.messaging;

public class MessagingSchoolMailClass {
    public MessagingSchoolMailClass(){}

    public String sender, title, time, content;

    public MessagingSchoolMailClass(String sender, String title, String time, String content){
        this.sender = sender;
        this.title = title;
        this.time = time;
        this.content = content;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }
}
