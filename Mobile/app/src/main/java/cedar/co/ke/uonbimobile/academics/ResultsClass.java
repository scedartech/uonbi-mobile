package cedar.co.ke.uonbimobile.academics;

public class ResultsClass {
    public ResultsClass(){}

    public String title, code, grade;

    public ResultsClass(String title, String code, String grade){
        this.title = title;
        this.code = code;
        this.grade = grade;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
