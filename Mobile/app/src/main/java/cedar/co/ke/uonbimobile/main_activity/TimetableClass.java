package cedar.co.ke.uonbimobile.main_activity;

public class TimetableClass {
    public TimetableClass(){}

    public String course, time;

    public TimetableClass(String course, String time){
        this.course = course;
        this.time = time;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
