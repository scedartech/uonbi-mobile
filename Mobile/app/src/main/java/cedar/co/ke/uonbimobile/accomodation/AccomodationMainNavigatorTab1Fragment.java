package cedar.co.ke.uonbimobile.accomodation;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;

public class AccomodationMainNavigatorTab1Fragment extends Fragment {
    private List<HostelBookingsClass> hostelBookingsList = new ArrayList<>();
    private HostelBookingsAdapter hostelBookingsAdapter;

    public AccomodationMainNavigatorTab1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_activity_tab1, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        hostelBookingsAdapter = new HostelBookingsAdapter(hostelBookingsList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(hostelBookingsAdapter);
        prepareRecyclerViewData();

        return rootView;
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>" +
                "<RESPONSE>"+
                "<BOOKINGS>"+
                "<BOOKING DATE='14 Sep 2015' STATUS='Room granted. Room 114 Hall 1' REMARKS='1'/>" +
                "<BOOKING DATE='18 Sep 2016' STATUS='Room granted. Room 71 Hall 2' REMARKS='1'/>" +
                "<BOOKING DATE='13 Sep 2017' STATUS='Request pending' REMARKS='0'/>" +
                "</BOOKINGS>" +
                "</RESPONSE>";
        HostelBookingsClass hostelBookingsClass;
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("BOOKING").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            hostelBookingsClass = new HostelBookingsClass(
                    (doc.getElementsByTagName("BOOKING").item(i).getAttributes().getNamedItem("DATE").getTextContent()).trim(),
                    (doc.getElementsByTagName("BOOKING").item(i).getAttributes().getNamedItem("STATUS").getTextContent()).trim(),
                    (doc.getElementsByTagName("BOOKING").item(i).getAttributes().getNamedItem("REMARKS").getTextContent()).trim()
            );
            hostelBookingsList.add(hostelBookingsClass);
        }
        hostelBookingsAdapter.notifyDataSetChanged();
    }
}
