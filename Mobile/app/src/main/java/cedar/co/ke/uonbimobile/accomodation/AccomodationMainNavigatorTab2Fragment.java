package cedar.co.ke.uonbimobile.accomodation;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.config.RecyclerTouchListener;

public class AccomodationMainNavigatorTab2Fragment extends Fragment {
    private List<SwaContactsClass> contactsList = new ArrayList<>();
    private SwaContactsAdapter contactsAdapter;

    public AccomodationMainNavigatorTab2Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_accomodation_main_navigator_tab2, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        contactsAdapter = new SwaContactsAdapter(contactsList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(contactsAdapter);

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                MaterialDialog callSeller = new MaterialDialog.Builder(getActivity())
                        .title("Call Contact")
                        .content("This will make a phone call to the contact listed. Call charges may apply.")
                        .canceledOnTouchOutside(false)
                        .positiveText("Call")
                        .negativeText("Cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                SwaContactsClass swaContactsClass = contactsList.get(position);

                                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + swaContactsClass.getContact()));
                                if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                            }
                        })
                        .show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        prepareRecyclerViewData();

        return rootView;
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>" +
                "<RESPONSE>"+
                "<SWA_CONTACTS>"+
                "<SWA_CONTACT TYPE='1' CONTACT='020-7897-4984' TITLE='Hall 1 Caretaker'/>" +
                "<SWA_CONTACT TYPE='1' CONTACT='020-4787-9879' TITLE='Hall 2 Caretaker'/>" +
                "<SWA_CONTACT TYPE='2' CONTACT='020-7894-4789' TITLE='Security office'/>" +
                "<SWA_CONTACT TYPE='3' CONTACT='0712478478' TITLE='Electrician'/>" +
                "<SWA_CONTACT TYPE='3' CONTACT='0714785658' TITLE='Janitor'/>" +
                "<SWA_CONTACT TYPE='3' CONTACT='0748787978' TITLE='Janitor'/>" +
                "</SWA_CONTACTS>" +
                "</RESPONSE>";
        SwaContactsClass contactsClass;
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("SWA_CONTACT").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            contactsClass = new SwaContactsClass(
                    (doc.getElementsByTagName("SWA_CONTACT").item(i).getAttributes().getNamedItem("TYPE").getTextContent()).trim(),
                    (doc.getElementsByTagName("SWA_CONTACT").item(i).getAttributes().getNamedItem("CONTACT").getTextContent()).trim(),
                    (doc.getElementsByTagName("SWA_CONTACT").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim()
            );
            contactsList.add(contactsClass);
        }
        contactsAdapter.notifyDataSetChanged();
    }
}
