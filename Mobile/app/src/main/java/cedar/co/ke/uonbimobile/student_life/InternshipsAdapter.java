package cedar.co.ke.uonbimobile.student_life;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;


public class InternshipsAdapter extends RecyclerView.Adapter<InternshipsAdapter.MyViewHolder> {

    private List<InternshipsClass> internshipsList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imgCompany;
        TextView company, title, description;

        public MyViewHolder(View itemView) {
            super(itemView);
            company = (TextView) itemView.findViewById(R.id.company);
            imgCompany = (ImageView) itemView.findViewById(R.id.imgCompany);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
        }
    }

    InternshipsAdapter(List<InternshipsClass> internshipsList){ this.internshipsList = internshipsList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_internship, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        InternshipsClass internshipsClass = internshipsList.get(position);
        holder.company.setText(internshipsClass.getCompany()+" - "+internshipsClass.getTime());
        holder.title.setText(internshipsClass.getTitle());
        holder.description.setText(internshipsClass.getDescription());
        if(internshipsClass.getCompany().equals("Global Tides Co.")){
            holder.imgCompany.setImageResource(R.drawable.global_tides);
        } else if(internshipsClass.getCompany().equals("SkyWorld Limited")) {
            holder.imgCompany.setImageResource(R.drawable.skyworld);
        } else if(internshipsClass.getCompany().equals("Centum Kenya LTD")) {
            holder.imgCompany.setImageResource(R.drawable.valuevest);
        }
    }

    @Override
    public int getItemCount() { return internshipsList.size();}
}
