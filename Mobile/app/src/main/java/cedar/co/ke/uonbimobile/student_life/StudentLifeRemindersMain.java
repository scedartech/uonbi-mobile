package cedar.co.ke.uonbimobile.student_life;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.login_register.LoginActivity;

public class StudentLifeRemindersMain extends AppCompatActivity {
    private List<RemindersClass> remindersList = new ArrayList<>();
    private RemindersAdapter remindersAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_life_reminders_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Reminders");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        remindersAdapter = new RemindersAdapter(remindersList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(remindersAdapter);
        prepareRecyclerViewData();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(StudentLifeRemindersMain.this, RemindersAddReminder.class));
            }
        });
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>"+
                "<REMINDERS>"+
                "<REMINDER TITLE='Homework' CONTENT='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.' TIME='27 Sep 2017'/>"+
                "<REMINDER TITLE='CAT Revision' CONTENT='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.' TIME='29 Sep 2017'/>"+
                "<REMINDER TITLE='Important' CONTENT='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.' TIME='29 Sep 2017'/>"+
                "<REMINDER TITLE='Android Progressive' CONTENT='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.' TIME='01 Oct 2017'/>"+
                "<REMINDER TITLE='iOS Tutorial Followup' CONTENT='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.' TIME='21 Oct 2017'/>"+
                "<REMINDER TITLE='Python Meetup Nairobi' CONTENT='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.' TIME='29 Oct 2017'/>"+
                "</REMINDERS>";
        RemindersClass remindersClass;
        DocumentBuilder builder = null;
        System.out.println(xml);
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("REMINDER").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            remindersClass = new RemindersClass(
                    (doc.getElementsByTagName("REMINDER").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim(),
                    (doc.getElementsByTagName("REMINDER").item(i).getAttributes().getNamedItem("CONTENT").getTextContent()).trim(),
                    (doc.getElementsByTagName("REMINDER").item(i).getAttributes().getNamedItem("TIME").getTextContent()).trim());
            remindersList.add(remindersClass);
        }
        remindersAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.subpage, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_help) {
            new MaterialDialog.Builder(this)
                    .title("Help")
                    .content("This page allows you to create an account that you can login with every time you want to use this mobile applicaton. Please note your username and password.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        }  else if(id == R.id.action_log_out){
            startActivity(new Intent(this, LoginActivity.class));
            ActivityCompat.finishAffinity(this);
        }

        return super.onOptionsItemSelected(item);
    }

    void exitQuestion(){
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Yes")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        ActivityCompat.finishAffinity(StudentLifeRemindersMain.this);
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, StudentLifeMainNavigator.class));
        ActivityCompat.finishAffinity(this);
    }
}
