package cedar.co.ke.uonbimobile.student_life;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.login_register.LoginActivity;

public class StudentLifeInternshipsMain extends AppCompatActivity {
    private List<InternshipsClass> internshipsList = new ArrayList<>();
    private InternshipsAdapter internshipsAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academics_results_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Internships");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        internshipsAdapter = new InternshipsAdapter(internshipsList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this.getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(internshipsAdapter);
        prepareRecyclerViewData();
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>"+
                "<INTERSHIPS>"+
                "<INTERSHIP COMPANY='Global Tides Co.' TIME='4 days ago' TITLE='Software Engineering Internship' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "<INTERSHIP COMPANY='SkyWorld Limited' TIME='3 hours ago' TITLE='Data Analyst Internship' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "<INTERSHIP COMPANY='Centum Kenya LTD' TIME='12 hours ago' TITLE='Forensic Studies Intern Needed' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "<INTERSHIP COMPANY='Global Tides Co.' TIME='8 days ago' TITLE='Software Engineer Wanted' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "<INTERSHIP COMPANY='Centum Kenya LTD' TIME='12 days ago' TITLE='Internship Position open' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "<INTERSHIP COMPANY='SkyWorld Limited' TIME='5 days ago' TITLE='4 month S/W Internship' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "<INTERSHIP COMPANY='Global Tides Co.' TIME='2 hours ago' TITLE='Computer Security Internship' DESCRIPTION='There has been a complete paradigm shift in global taxation with the rise in audit risks caused by new global initiatives like the OECD’s BEPS Project.'/>"+
                "</INTERSHIPS>";
        InternshipsClass internshipsClass;
        DocumentBuilder builder = null;
        System.out.println(xml);
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("INTERSHIP").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            internshipsClass = new InternshipsClass(
                    (doc.getElementsByTagName("INTERSHIP").item(i).getAttributes().getNamedItem("COMPANY").getTextContent()).trim(),
                    (doc.getElementsByTagName("INTERSHIP").item(i).getAttributes().getNamedItem("TIME").getTextContent()).trim(),
                    (doc.getElementsByTagName("INTERSHIP").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim(),
                    (doc.getElementsByTagName("INTERSHIP").item(i).getAttributes().getNamedItem("DESCRIPTION").getTextContent()).trim());
            internshipsList.add(internshipsClass);
        }
        internshipsAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.subpage, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_help) {
            new MaterialDialog.Builder(this)
                    .title("Help")
                    .content("This page allows you to create an account that you can login with every time you want to use this mobile applicaton. Please note your username and password.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        }  else if(id == R.id.action_log_out){
            startActivity(new Intent(this, LoginActivity.class));
            ActivityCompat.finishAffinity(this);
        }

        return super.onOptionsItemSelected(item);
    }

    void exitQuestion(){
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Yes")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        ActivityCompat.finishAffinity(StudentLifeInternshipsMain.this);
                    }
                })
                .show();
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, StudentLifeMainNavigator.class));
        ActivityCompat.finishAffinity(this);
    }
}
