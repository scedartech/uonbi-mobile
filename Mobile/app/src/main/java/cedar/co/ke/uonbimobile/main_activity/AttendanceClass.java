package cedar.co.ke.uonbimobile.main_activity;

public class AttendanceClass {
    public AttendanceClass(){}

    public String title, code;
    int percentage;

    public AttendanceClass(String title, String code, int percentage){
        this.title = title;
        this.code = code;
        this.percentage = percentage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPercentage() {
        return percentage;
    }

    public void setPercentage(int percentage) {
        this.percentage = percentage;
    }
}
