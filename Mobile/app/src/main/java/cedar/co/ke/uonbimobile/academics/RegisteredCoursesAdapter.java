package cedar.co.ke.uonbimobile.academics;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class RegisteredCoursesAdapter extends RecyclerView.Adapter<RegisteredCoursesAdapter.MyViewHolder> {

    private List<RegisteredCoursesClass> coursesList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout llPercentage;
        TextView title, code, percentage;

        public MyViewHolder(View itemView) {
            super(itemView);
            llPercentage = (LinearLayout) itemView.findViewById(R.id.llPercentage);
            title = (TextView) itemView.findViewById(R.id.title);
            code = (TextView) itemView.findViewById(R.id.code);
            percentage = (TextView) itemView.findViewById(R.id.percentage);
        }
    }

    RegisteredCoursesAdapter(List<RegisteredCoursesClass> coursesList){ this.coursesList = coursesList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewPercentage) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_course_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RegisteredCoursesClass coursesClass = coursesList.get(position);
        holder.title.setText(coursesClass.getTitle());
        holder.code.setText(coursesClass.getCode());
    }

    @Override
    public int getItemCount() { return coursesList.size();}
}
