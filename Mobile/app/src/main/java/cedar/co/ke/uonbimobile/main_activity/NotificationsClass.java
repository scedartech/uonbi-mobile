package cedar.co.ke.uonbimobile.main_activity;

public class NotificationsClass {
    public NotificationsClass(){}

    public String type, title, time, content;

    public NotificationsClass(String type, String title, String time, String content){
        this.type = type;
        this.title = title;
        this.time = time;
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
