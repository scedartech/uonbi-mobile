package cedar.co.ke.uonbimobile.accomodation;

public class SwaContactsClass {
    public SwaContactsClass(){}

    public String type, title, contact;

    public SwaContactsClass(String type, String contact, String title){
        this.type = type;
        this.title = title;
        this.contact = contact;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
