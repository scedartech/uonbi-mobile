package cedar.co.ke.uonbimobile.accomodation;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;


public class SwaContactsAdapter extends RecyclerView.Adapter<SwaContactsAdapter.MyViewHolder> {

    private List<SwaContactsClass> swaContactsList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView type;
        TextView title, contact;

        public MyViewHolder(View itemView) {
            super(itemView);
            type = (ImageView) itemView.findViewById(R.id.type);
            title = (TextView) itemView.findViewById(R.id.title);
            contact = (TextView) itemView.findViewById(R.id.contact);
        }
    }

    SwaContactsAdapter(List<SwaContactsClass> swaContactsList){ this.swaContactsList = swaContactsList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_swa_contact_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SwaContactsClass swaContactsClass = swaContactsList.get(position);
        holder.title.setText(swaContactsClass.getTitle());
        holder.contact.setText(swaContactsClass.getContact());
        if(swaContactsClass.getType().equals("1")){
            holder.type.setImageResource(R.drawable.ic_urgent);
        } else if(swaContactsClass.getType().equals("2")) {
            holder.type.setImageResource(R.drawable.ic_reminder);
        } else if(swaContactsClass.getType().equals("3")) {
            holder.type.setImageResource(R.drawable.ic_meeting);
        }

    }

    @Override
    public int getItemCount() { return swaContactsList.size();}
}
