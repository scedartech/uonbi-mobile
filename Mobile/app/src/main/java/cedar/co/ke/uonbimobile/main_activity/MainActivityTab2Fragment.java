package cedar.co.ke.uonbimobile.main_activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;

public class MainActivityTab2Fragment extends Fragment {
    private List<TimetableClass> mondayTimetableList = new ArrayList<>();
    private List<TimetableClass> tuesdayTimetableList = new ArrayList<>();
    private List<TimetableClass> wednesdayTimetableList = new ArrayList<>();
    private List<TimetableClass> thursdayTimetableList = new ArrayList<>();
    private List<TimetableClass> fridayTimetableList = new ArrayList<>();
    private TimetableAdapter mondayTimetableAdapter, tuesdayTimetableAdapter, wednesdayTimetableAdapter, thursdayTimetableAdapter, fridayTimetableAdapter;
    View rootView;
    RecyclerView mondayRecyclerView, tuesdayRecyclerView, wednesdayRecyclerView, thursdayRecyclerView, fridayRecyclerView;
    RecyclerView.LayoutManager mondayLayoutManager, tuesdayLayoutManager, wednesdayLayoutManager, thursdayLayoutManager, fridayLayoutManager;

    public MainActivityTab2Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_main_activity_tab2, container, false);

        for(int dayofWeek = 0; dayofWeek < 5; dayofWeek++) {
            prepareRecyclerViewData(dayofWeek);
            //timetableList.clear();
        }
        return rootView;
    }

    private void prepareRecyclerViewData(int dayofWeek){
        String xml = ""+
        "<?xml version='1.0'?>"+
        "<RESPONSE>"+
        "<TIMETABLE_ITEMS>"+
        "<DAY>"+
        "<TIMETABLE_ITEM COURSE='CSC311 - Analysis And Design Of Algorithms' TIME='9.00 am - 10.45 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC312 - Artificial Intelligence Programming' TIME='11.15 am - 1.00 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC313 - Foundations Of Human Computer Interaction' TIME='2.00 pm - 3.45 pm'/>"+
        "</DAY>"+
        "<DAY>"+
        "<TIMETABLE_ITEM COURSE='CSC314 - Computer Graphics' TIME='9.00 am - 10.45 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC313 - Foundations Of Human Computer Interaction' TIME='11.15 am - 1.00 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC312 - Artificial Intelligence Programming' TIME='2.00 pm - 3.45 pm'/>"+
        "</DAY>"+
        "<DAY>"+
        "<TIMETABLE_ITEM COURSE='CSC315 - Distributed Systems' TIME='9.00 am - 10.45 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC312 - Artificial Intelligence Programming' TIME='11.15 am - 1.00 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC313 - Foundations Of Human Computer Interaction' TIME='2.00 pm - 3.45 pm'/>"+
        "<TIMETABLE_ITEM COURSE='CSC314 - Computer Graphics' TIME='4.00 pm - 5.45 pm'/>"+
        "</DAY>"+
        "<DAY>"+
        "<TIMETABLE_ITEM COURSE='CSC311 - Analysis And Design Of Algorithms' TIME='9.00 am - 10.45 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC312 - Artificial Intelligence Programming' TIME='11.15 am - 1.00 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC313 - Foundations Of Human Computer Interaction' TIME='2.00 pm - 3.45 pm'/>"+
        "</DAY>"+
        "<DAY>"+
        "<TIMETABLE_ITEM COURSE='CSC314 - Introduction To Organizations And Management' TIME='9.00 am - 10.45 am'/>"+
        "<TIMETABLE_ITEM COURSE='CSC314 - Introduction To Organizations And Management' TIME='11.15 am - 1.00 am'/>"+
        "</DAY>"+
        "</TIMETABLE_ITEMS>"+
        "</RESPONSE>";

        TimetableClass mondayTimetableClass, tuesdayTimetableClass, wednesdayTimetableClass, thursdayTimetableClass, fridayTimetableClass;
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().getLength();
            System.out.println("itemCount is "+itemCount);
        }

        if (dayofWeek == 0) {
            mondayRecyclerView = (RecyclerView) rootView.findViewById(R.id.mondayRecyclerView);
            mondayTimetableAdapter = new TimetableAdapter(mondayTimetableList);
            mondayRecyclerView.setHasFixedSize(true);
            mondayLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            mondayRecyclerView.setLayoutManager(mondayLayoutManager);
            mondayRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            mondayRecyclerView.setItemAnimator(new DefaultItemAnimator());
            mondayRecyclerView.setAdapter(mondayTimetableAdapter);

            for(int i = 0; i < itemCount; i++){
                mondayTimetableClass = new TimetableClass(
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("COURSE").getTextContent().trim(),
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("TIME").getTextContent().trim()
                );
                mondayTimetableList.add(mondayTimetableClass);
            }
            mondayTimetableAdapter.notifyDataSetChanged();
        } else if (dayofWeek == 1) {
            tuesdayRecyclerView = (RecyclerView) rootView.findViewById(R.id.tuesdayRecyclerView);
            tuesdayTimetableAdapter = new TimetableAdapter(tuesdayTimetableList);
            tuesdayRecyclerView.setHasFixedSize(true);
            tuesdayLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            tuesdayRecyclerView.setLayoutManager(tuesdayLayoutManager);
            tuesdayRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            tuesdayRecyclerView.setItemAnimator(new DefaultItemAnimator());
            tuesdayRecyclerView.setAdapter(tuesdayTimetableAdapter);

            for(int i = 0; i < itemCount; i++){
                tuesdayTimetableClass = new TimetableClass(
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("COURSE").getTextContent().trim(),
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("TIME").getTextContent().trim()
                );
                tuesdayTimetableList.add(tuesdayTimetableClass);
            }
            tuesdayTimetableAdapter.notifyDataSetChanged();
        } else if (dayofWeek == 2) {
            wednesdayRecyclerView = (RecyclerView) rootView.findViewById(R.id.wednesdayRecyclerView);
            wednesdayTimetableAdapter = new TimetableAdapter(wednesdayTimetableList);
            wednesdayRecyclerView.setHasFixedSize(true);
            wednesdayLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            wednesdayRecyclerView.setLayoutManager(wednesdayLayoutManager);
            wednesdayRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            wednesdayRecyclerView.setItemAnimator(new DefaultItemAnimator());
            wednesdayRecyclerView.setAdapter(wednesdayTimetableAdapter);

            for(int i = 0; i < itemCount; i++){
                wednesdayTimetableClass = new TimetableClass(
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("COURSE").getTextContent().trim(),
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("TIME").getTextContent().trim()
                );
                wednesdayTimetableList.add(wednesdayTimetableClass);
            }
            wednesdayTimetableAdapter.notifyDataSetChanged();
        } else if (dayofWeek == 3) {
            thursdayRecyclerView = (RecyclerView) rootView.findViewById(R.id.thursdayRecyclerView);
            thursdayTimetableAdapter = new TimetableAdapter(thursdayTimetableList);
            thursdayRecyclerView.setHasFixedSize(true);
            thursdayLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            thursdayRecyclerView.setLayoutManager(thursdayLayoutManager);
            thursdayRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            thursdayRecyclerView.setItemAnimator(new DefaultItemAnimator());
            thursdayRecyclerView.setAdapter(thursdayTimetableAdapter);

            for(int i = 0; i < itemCount; i++){
                thursdayTimetableClass = new TimetableClass(
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("COURSE").getTextContent().trim(),
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("TIME").getTextContent().trim()
                );
                thursdayTimetableList.add(thursdayTimetableClass);
            }
            thursdayTimetableAdapter.notifyDataSetChanged();
        } else if (dayofWeek == 4) {
            fridayRecyclerView = (RecyclerView) rootView.findViewById(R.id.fridayRecyclerView);
            fridayTimetableAdapter = new TimetableAdapter(fridayTimetableList);
            fridayRecyclerView.setHasFixedSize(true);
            fridayLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            fridayRecyclerView.setLayoutManager(fridayLayoutManager);
            fridayRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
            fridayRecyclerView.setItemAnimator(new DefaultItemAnimator());
            fridayRecyclerView.setAdapter(fridayTimetableAdapter);

            for(int i = 0; i < itemCount; i++){
                fridayTimetableClass = new TimetableClass(
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("COURSE").getTextContent().trim(),
                        doc.getElementsByTagName("DAY").item(dayofWeek).getChildNodes().item(i).getAttributes().getNamedItem("TIME").getTextContent().trim()
                );
                fridayTimetableList.add(fridayTimetableClass);
            }
            fridayTimetableAdapter.notifyDataSetChanged();
        }
    }
}
