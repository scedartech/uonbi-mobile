package cedar.co.ke.uonbimobile.main_activity;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {

    private List<NotificationsClass> notificationsList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView type;
        TextView title, time, content;

        public MyViewHolder(View itemView) {
            super(itemView);
            type = (ImageView) itemView.findViewById(R.id.type);
            title = (TextView) itemView.findViewById(R.id.title);
            time = (TextView) itemView.findViewById(R.id.time);
            content = (TextView) itemView.findViewById(R.id.content);
        }
    }

    NotificationsAdapter(List<NotificationsClass> notificationsList){ this.notificationsList = notificationsList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_main_activity_notification, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationsClass notificationsClass = notificationsList.get(position);
        holder.title.setText(notificationsClass.getTitle());
        holder.time.setText(notificationsClass.getTime());
        holder.content.setText(notificationsClass.getContent());
        if(notificationsClass.getType().equals("1")){
            holder.type.setImageResource(R.drawable.ic_urgent);
        } else if(notificationsClass.getType().equals("2")) {
            holder.type.setImageResource(R.drawable.ic_reminder);
        } else if(notificationsClass.getType().equals("3")) {
            holder.type.setImageResource(R.drawable.ic_meeting);
        }

    }

    @Override
    public int getItemCount() { return notificationsList.size();}
}
