package cedar.co.ke.uonbimobile.main_activity;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class AttendanceAdapter extends RecyclerView.Adapter<AttendanceAdapter.MyViewHolder> {

    private List<AttendanceClass> attendanceList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout llPercentage;
        TextView title, code, percentage;

        public MyViewHolder(View itemView) {
            super(itemView);
            llPercentage = (LinearLayout) itemView.findViewById(R.id.llPercentage);
            title = (TextView) itemView.findViewById(R.id.title);
            code = (TextView) itemView.findViewById(R.id.code);
            percentage = (TextView) itemView.findViewById(R.id.percentage);
        }
    }

    AttendanceAdapter(List<AttendanceClass> attendanceList){ this.attendanceList = attendanceList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewPercentage) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_attendance_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        AttendanceClass attendanceClass = attendanceList.get(position);
        holder.title.setText(attendanceClass.getTitle());
        holder.code.setText(attendanceClass.getCode());
        holder.percentage.setText(attendanceClass.getPercentage()+"%");
        int percentage = attendanceClass.getPercentage();


        ViewGroup.LayoutParams params = holder.llPercentage.getLayoutParams();
        params.width = ((int)((MainActivity.windowWidth - 16) * ((float)(attendanceClass.getPercentage()))) / 100);
        holder.llPercentage.setLayoutParams(params);

        //holder.llPercentage.setMinimumWidth((int)(MainActivity.windowWidth * ((float)(attendanceClass.getPercentage()))) / 100);
        if(percentage <= 100 && percentage >=90){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance90);
        } else if(percentage < 90 && percentage >=80){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance80);
        } else if(percentage < 80 && percentage >=70){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance70);
        } else if(percentage < 70 && percentage >=60){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance60);
        } else if(percentage < 60 && percentage >=50){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance50);
        } else if(percentage < 50 && percentage >=40){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance40);
        } else if(percentage < 40 && percentage >=30){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance30);
        } else if(percentage < 30 && percentage >=20){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance20);
        } else if(percentage < 20 && percentage >= 0){
            holder.llPercentage.setBackgroundResource(R.color.colorAttencance10);
        }

        System.out.println("width is "+((int)(MainActivity.windowWidth * ((float)(attendanceClass.getPercentage()))) / 100));
    }

    @Override
    public int getItemCount() { return attendanceList.size();}
}
