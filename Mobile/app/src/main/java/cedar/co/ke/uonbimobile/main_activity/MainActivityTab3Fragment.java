package cedar.co.ke.uonbimobile.main_activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;

public class MainActivityTab3Fragment extends Fragment {
    private List<AttendanceClass> attendanceList = new ArrayList<>();
    private AttendanceAdapter attendanceAdapter;

    public MainActivityTab3Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_activity_tab3, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        attendanceAdapter = new AttendanceAdapter(attendanceList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(attendanceAdapter);
        prepareRecyclerViewData();

        return rootView;
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>"+
                "<COURSES>"+
                "<COURSE TITLE='Analysis and design of algorithms' CODE='CSC311' PERCENTAGE='51'/>"+
                "<COURSE TITLE='Artificial Intelligence Programming' CODE='CSC312' PERCENTAGE='63'/>"+
                "<COURSE TITLE='Foundations of HCI' CODE='CSC313' PERCENTAGE='19'/>"+
                "<COURSE TITLE='Computer Graphics' CODE='CSC314' PERCENTAGE='75'/>"+
                "<COURSE TITLE='Distributed Systems' CODE='CSC315' PERCENTAGE='63'/>"+
                "<COURSE TITLE='Introductions to Orgs and Mgmt.' CODE='CSC316' PERCENTAGE='89'/>"+
                "</COURSES>";
        AttendanceClass attendanceClass;
        DocumentBuilder builder = null;
        System.out.println(xml);
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("COURSE").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            attendanceClass = new AttendanceClass(
                    (doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim(),
                    (doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("CODE").getTextContent()).trim(),
                    Integer.valueOf(doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("PERCENTAGE").getTextContent().trim())
            );
            System.out.println("title is "+doc.getElementsByTagName("COURSE").item(i).getAttributes().getNamedItem("TITLE").getTextContent());
            attendanceList.add(attendanceClass);
        }
        attendanceAdapter.notifyDataSetChanged();
    }
}
