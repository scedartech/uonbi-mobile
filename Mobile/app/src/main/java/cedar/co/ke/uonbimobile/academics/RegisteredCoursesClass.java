package cedar.co.ke.uonbimobile.academics;

public class RegisteredCoursesClass {
    public RegisteredCoursesClass(){

    }

    public String title, code;

    public RegisteredCoursesClass(String title, String code){
        this.title = title;
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
