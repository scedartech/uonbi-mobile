package cedar.co.ke.uonbimobile.accomodation;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class HostelBookingsAdapter extends RecyclerView.Adapter<HostelBookingsAdapter.MyViewHolder> {

    private List<HostelBookingsClass> hostelBookingsList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        LinearLayout llStatus;
        TextView validity, status, date;

        public MyViewHolder(View itemView) {
            super(itemView);
            status = (TextView) itemView.findViewById(R.id.status);
            date = (TextView) itemView.findViewById(R.id.date);
            llStatus = (LinearLayout) itemView.findViewById(R.id.llStatus);
        }
    }

    HostelBookingsAdapter(List<HostelBookingsClass> hostelBookingsList){ this.hostelBookingsList = hostelBookingsList; }

    @Override
    public HostelBookingsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewGrade) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hostel_booking, parent, false);

        return new HostelBookingsAdapter.MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(HostelBookingsAdapter.MyViewHolder holder, int position) {
        HostelBookingsClass hostelBookingsClass = hostelBookingsList.get(position);
        holder.status.setText(hostelBookingsClass.getStatus());
        holder.date.setText(hostelBookingsClass.getDate());

        if(hostelBookingsClass.getRemarks().equals("1")){
            holder.llStatus.setBackgroundResource(R.color.colorAttencance90);
        } else {
            holder.llStatus.setBackgroundResource(R.color.colorAttencance40);
        }
    }

    @Override
    public int getItemCount() { return hostelBookingsList.size();}
}
