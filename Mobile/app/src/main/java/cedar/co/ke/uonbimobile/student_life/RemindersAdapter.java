package cedar.co.ke.uonbimobile.student_life;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import cedar.co.ke.uonbimobile.R;

public class RemindersAdapter extends RecyclerView.Adapter<RemindersAdapter.MyViewHolder> {

    private List<RemindersClass> remindersList;

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView title, content, time;

        public MyViewHolder(View itemView) {
            super(itemView);
            time = (TextView) itemView.findViewById(R.id.time);
            content = (TextView) itemView.findViewById(R.id.content);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }

    RemindersAdapter(List<RemindersClass> remindersList){ this.remindersList = remindersList; }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_reminders, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        RemindersClass remindersClass = remindersList.get(position);
        holder.title.setText(remindersClass.getTitle());
        holder.content.setText(remindersClass.getContent());
        holder.time.setText(remindersClass.getTime());
    }

    @Override
    public int getItemCount() { return remindersList.size();}
}