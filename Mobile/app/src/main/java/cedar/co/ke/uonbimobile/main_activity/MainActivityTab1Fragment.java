package cedar.co.ke.uonbimobile.main_activity;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import cedar.co.ke.uonbimobile.R;

public class MainActivityTab1Fragment extends Fragment {
    private List<NotificationsClass> notificationsList = new ArrayList<>();
    private NotificationsAdapter notificationsAdapter;

    public MainActivityTab1Fragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_activity_tab1, container, false);

        RecyclerView recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        notificationsAdapter = new NotificationsAdapter(notificationsList);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        //recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(notificationsAdapter);
        prepareRecyclerViewData();

        return rootView;
    }

    private void prepareRecyclerViewData() {
        String xml = "<?xml version='1.0'?>" +
        "<RESPONSE>"+
        "<NOTIFICATIONS>"+
        "<NOTIFICATION TYPE='1' TITLE='In the data files' TIME='12.43 PM' CONTENT='In the element, the format file represents the data values in all three fields as character data. For each field, the TERMINATOR attribute indicates the terminator that follows the data value.'/>" +
        "<NOTIFICATION TYPE='2' TITLE='The following example' TIME='9.15 AM' CONTENT='It will also take text and attribute arguments (after the tag name) like normal markup methods. (But see the next bullet item for a better way to handle XML namespaces). Direct support for XML namespaces is now available. If the first argument to a tag call is a symbol, it will be joined to the tag to produce a namespace combination. It is easier to show this than describe it.'/>" +
        "<NOTIFICATION TYPE='1' TITLE='Table (row)' TIME='12.56 PM' CONTENT='The instance_eval implementation which forces self to refer to the message receiver as self is now obsolete. We now use normal block calls to execute the markup block. This means that all markup methods must now be explicitly send to the xml builder. For instance, instead of '/>" +
        "<NOTIFICATION TYPE='3' TITLE='The data fields' TIME='12.43 PM' CONTENT='Although more verbose, the subtle change in semantics within the block was found to be prone to error. To make this change a little less cumbersome, the markup block now gets the markup object sent as an argument, allowing you to use a shorter alias within the block. '/>" +
        "<NOTIFICATION TYPE='2' TITLE='Ordering data fields' TIME='5.43 PM' CONTENT='The special XML characters automatically. Use the operation to insert text without modification.'/>" +
        "<NOTIFICATION TYPE='1' TITLE='The following example' TIME='6.43 AM' CONTENT='Create XML markup easily. All (well, almost all) methods sent to an XmlMarkup object will be translated to the equivalent XML markup. Any method with a block will be treated as an XML markup tag with nested markup in the block. '/>" +
        "</NOTIFICATIONS>" +
        "</RESPONSE>";
        NotificationsClass notificationsClass;
        DocumentBuilder builder = null;
        try {
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        InputSource src = new InputSource();
        src.setCharacterStream(new StringReader(xml));
        Document doc = null;
        try {
            if (builder != null) {
                doc = builder.parse(src);
            }
        } catch (SAXException | IOException e) {
            e.printStackTrace();
        }

        int itemCount = 0;
        if (doc != null) {
            itemCount = doc.getElementsByTagName("NOTIFICATION").getLength();
            System.out.println("itemCount is "+itemCount);
        }

        for(int i = 0; i < itemCount; i++){
            notificationsClass = new NotificationsClass(
                (doc.getElementsByTagName("NOTIFICATION").item(i).getAttributes().getNamedItem("TYPE").getTextContent()).trim(),
                (doc.getElementsByTagName("NOTIFICATION").item(i).getAttributes().getNamedItem("TITLE").getTextContent()).trim(),
                (doc.getElementsByTagName("NOTIFICATION").item(i).getAttributes().getNamedItem("TIME").getTextContent()).trim(),
                (doc.getElementsByTagName("NOTIFICATION").item(i).getAttributes().getNamedItem("CONTENT").getTextContent()).trim()
            );
            notificationsList.add(notificationsClass);
        }
        notificationsAdapter.notifyDataSetChanged();
    }
}
