package cedar.co.ke.uonbimobile.login_register;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import cedar.co.ke.uonbimobile.R;

public class Register extends AppCompatActivity {
    int LOGIN_TIME_OUT = 3000;
    EditText txPINNumber;
    EditText txPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        final Button btnCreateAccount = (Button) findViewById(R.id.btnCreateAccount);

        final TextView tvLoginHeader = (TextView) findViewById(R.id.tvLoginHeader);
        final TextView tvHelpReg = (TextView) findViewById(R.id.tvHelpReg);
        final TextInputLayout tilRefNo = (TextInputLayout) findViewById(R.id.tilRefNo);
        final EditText txRefNo = (EditText) findViewById(R.id.txRefNo);
        final TextView tvResponseText = (TextView) findViewById(R.id.tvResponseText);
        final TextView tvResponseHeader = (TextView) findViewById(R.id.tvResponseHeader);
        final TextView tvResponseDesc = (TextView) findViewById(R.id.tvResponseDesc);

        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final ProgressBar pbLoadSpinner = (ProgressBar) findViewById(R.id.pbLoadSpinner);
                pbLoadSpinner.setVisibility(View.VISIBLE);

                new Handler().postDelayed(new Runnable() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void run() {
                        pbLoadSpinner.setVisibility(View.GONE);
                        tvLoginHeader.setVisibility(View.GONE);
                        tvHelpReg.setVisibility(View.GONE);
                        tilRefNo.setVisibility(View.GONE);
                        txRefNo.setVisibility(View.GONE);
                        tvResponseText.setText("You Regstration Number is");
                        tvResponseHeader.setText("P15/37217/2016");
                        tvResponseDesc.setText("This is the Registration Number you will be using for all university services and to log in to this app every time. Note it somewhere then press the back button below to log in.");
                        btnCreateAccount.setEnabled(false);
                        btnCreateAccount.setTextColor(getResources().getColor(R.color.colorSpalsh));
                    }
                }, LOGIN_TIME_OUT);
            }
        });

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Register.this, LoginActivity.class));
                ActivityCompat.finishAffinity(Register.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.login_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_exit:
                exit();
                break;
            case R.id.action_help:
                help();
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(Register.this, LoginActivity.class));
        ActivityCompat.finishAffinity(this);
    }

    public void exit(){
        MaterialDialog dialogExit = new MaterialDialog.Builder(Register.this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Exit")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        ActivityCompat.finishAffinity(Register.this);
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                    }
                })
                .show();
    }

    public void help(){
        MaterialDialog dialogHelp = new MaterialDialog.Builder(Register.this)
                .title("Help")
                .content("University of Nairobi Register page help content.")
                .negativeText("Close")
                .canceledOnTouchOutside(false)
                .show();
    }
}
