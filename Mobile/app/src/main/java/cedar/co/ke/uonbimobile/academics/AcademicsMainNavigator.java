package cedar.co.ke.uonbimobile.academics;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import cedar.co.ke.uonbimobile.R;
import cedar.co.ke.uonbimobile.accomodation.AccomodationMainNavigator;
import cedar.co.ke.uonbimobile.login_register.LoginActivity;
import cedar.co.ke.uonbimobile.main_activity.MainActivity;
import cedar.co.ke.uonbimobile.messaging.MessagingMain;
import cedar.co.ke.uonbimobile.settings.SettingsMainNavigator;
import cedar.co.ke.uonbimobile.student_life.StudentLifeMainNavigator;

public class AcademicsMainNavigator extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    ConstraintLayout courseRegistrationConstraintLayout,timetablesConstraintLayout, resultsConstraintLayout, studentIdConstraintLayout, examCardConstraintLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_academics_main_navigator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Academics");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        DrawerLayout mDrawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));

        courseRegistrationConstraintLayout = (ConstraintLayout) findViewById(R.id.courseRegistrationConstraintLayout);
        timetablesConstraintLayout = (ConstraintLayout) findViewById(R.id.timetablesConstraintLayout);
        resultsConstraintLayout = (ConstraintLayout) findViewById(R.id.resultsConstraintLayout);
        studentIdConstraintLayout = (ConstraintLayout) findViewById(R.id.studentIdConstraintLayout);
        examCardConstraintLayout = (ConstraintLayout) findViewById(R.id.examCardConstraintLayout);

        courseRegistrationConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AcademicsMainNavigator.this, AcademicsCourseRegistrationMain.class));
            }
        });

        timetablesConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AcademicsMainNavigator.this, AcademicsTimetables.class));
            }
        });

        resultsConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AcademicsMainNavigator.this, AcademicsResultsPin.class));
            }
        });

        studentIdConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AcademicsMainNavigator.this, AcademicsStudentId.class));
            }
        });

        examCardConstraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MaterialDialog dialog = new MaterialDialog.Builder(AcademicsMainNavigator.this)
                        .title("Download exam card")
                        .content("Do you wish to download you semester examination card?")
                        .canceledOnTouchOutside(false)
                        .positiveText("Download")
                        .negativeText("Cancel")
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.hide();
                                final MaterialDialog progressDialog = new MaterialDialog.Builder(AcademicsMainNavigator.this)
                                        .title("Downloading exam card")
                                        .content("Please wait")
                                        .progress(true, 100)
                                        .cancelable(false)
                                        .progressIndeterminateStyle(true)
                                        .show();

                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.hide();
                                        final MaterialDialog dialog = new MaterialDialog.Builder(AcademicsMainNavigator.this)
                                                .title("Download successful")
                                                .content("Exam card downloaded successfully. Please check you phone's internal storage for the pdf file.")
                                                .canceledOnTouchOutside(false)
                                                .negativeText("Close")
                                                .show();
                                    }
                                }, 2400);
                            }
                        })
                        .show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            startActivity(new Intent(this, MainActivity.class));
            ActivityCompat.finishAffinity(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_help) {
            new MaterialDialog.Builder(this)
                    .title("Help")
                    .content("This page allows you to create an account that you can login with every time you want to use this mobile applicaton. Please note your username and password.")
                    .canceledOnTouchOutside(false)
                    .negativeText("Close")
                    .show();
        } else if (id == R.id.action_exit) {
            exitQuestion();
        } else if(id == R.id.action_log_out){
            startActivity(new Intent(this, LoginActivity.class));
            ActivityCompat.finishAffinity(this);
        }

        return super.onOptionsItemSelected(item);
    }

    void exitQuestion(){
        new MaterialDialog.Builder(this)
                .title("Exit")
                .content("Do you want to exit?")
                .positiveText("Yes")
                .negativeText("Cancel")
                .canceledOnTouchOutside(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        Intent homeIntent = new Intent(Intent.ACTION_MAIN);
                        homeIntent.addCategory( Intent.CATEGORY_HOME );
                        homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(homeIntent);
                        ActivityCompat.finishAffinity(AcademicsMainNavigator.this);
                    }
                })
                .show();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.

        if (item.getItemId() == R.id.nav_home) {
            startActivity(new Intent(this, MainActivity.class));
            ActivityCompat.finishAffinity(this);
        } else if(item.getItemId() == R.id.nav_academics){

        } else if(item.getItemId() == R.id.nav_accomodation){
            startActivity(new Intent(this, AccomodationMainNavigator.class));
            ActivityCompat.finishAffinity(this);
        } else if(item.getItemId() == R.id.nav_student_life){
            startActivity(new Intent(this, StudentLifeMainNavigator.class));
            ActivityCompat.finishAffinity(this);
        } else if(item.getItemId() == R.id.nav_messages){
            startActivity(new Intent(this, MessagingMain.class));
            ActivityCompat.finishAffinity(this);
        } else if(item.getItemId() == R.id.nav_settings){
            startActivity(new Intent(this, SettingsMainNavigator.class));
            ActivityCompat.finishAffinity(this);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
